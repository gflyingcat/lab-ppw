 $( document ).ready(function() {
  var button_8 = $('button:contains("8")');
  var button_4 = $('button:contains("4")');

  var button_add = $('button:contains("+")');
  var button_sub = $('button:contains("-")');
  var button_mul = $('button:contains("*")');
  var button_div = $('button:contains("/")');

  var button_sin = $('button:contains("sin")');
  var button_tan = $('button:contains("tan")');
  var button_log = $('button:contains("log")');

  var button_clear = $('button:contains("AC")');
  var button_res = $('button:contains("=")');

  var chatBox = $('.msg-insert');
  var chatBoxInput = $('#msgarea');

  // not done yet due to async calls or fail trigger
  // QUnit.test("Input Chatbox Text", function(assert) {
  //   var tmp = window.localStorage.getItem('chatLog');
  //   chatBoxInput.val('ini test, mohon sabar');
  //   chatBoxInput.trigger({
  //     type: 'keypress', keyCode: 13, which: 13, charCode: 13
  //   });
  //   assert.notEqual(chatBox.val().indexOf('ini test, mohon sabar'), -1);
  //   window.localStorage.setItem('chatLog', tmp);
  // });
  QUnit.test("Functions test", function(assert) {
    button_8.click();
    button_sin.click();
    assert.equal( $('#print').val(), Math.sin(8), "must be same as Math.sin" );
    button_clear.click();

    button_8.click();
    button_tan.click();
    assert.equal( $('#print').val(), Math.tan(8), "must be same as Math.sin" );
    button_clear.click();

    button_8.click();
    button_log.click();
    assert.equal( $('#print').val(), Math.log(8), "must be same as Math.sin" );
    button_clear.click();
  });

  QUnit.test( "Addition Test", function( assert ) {
    button_8.click();
    button_add.click();
    button_4.click();
    button_res.click();
    assert.equal( $('#print').val(), 12, "8 + 4 must be 12" );
    button_clear.click();
  });

  QUnit.test( "Substraction Test", function( assert ) {
    button_8.click();
    button_sub.click();
    button_4.click();
    button_res.click();
    assert.equal( $('#print').val(), 4, "8 - 4 must be 4" );
    button_clear.click();
  });

  QUnit.test( "Multiply Test", function( assert ) {
    button_8.click();
    button_mul.click();
    button_4.click();
    button_res.click();
    assert.equal( $('#print').val(), 32, "8 * 4 must be 32" );
    button_clear.click();
  });

  QUnit.test( "Division Test", function( assert ) {
    button_8.click();
    button_div.click();
    button_4.click();
    button_res.click();
    assert.equal( $('#print').val(), 2, "8 / 4 must be 2" );
    button_clear.click();
  });
});
