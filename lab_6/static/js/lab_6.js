var print, msgarea;

var go = function(x) {
  if (x === 'sin') {
    print.value = Math.sin(eval(print.value));
  } else if (x === 'log') {
    print.value = Math.log(eval(print.value));
  } else if (x === 'tan') {
    print.value = Math.tan(eval(print.value));
  } else if (x === 'ac') {
    print.value = '';
  } else if (x === 'eval') {
    print.value = eval(print.value);
  } else {
    print.value += x;
  }
};


function eval(val) {
  return Math.round(evil(print.value) * 10000) / 10000;
}
function evil(fn) {
  return new Function('return ' + fn)();
}

function appendChatLog(chat) {
  var prevLog = window.localStorage.getItem('chatLog') || '[]';
  prevLog = JSON.parse(prevLog);
  prevLog.push(chat);
  window.localStorage.setItem('chatLog', JSON.stringify(prevLog));
}

function showChatLog() {
  var log = window.localStorage.getItem('chatLog') || '[]';
  log = JSON.parse(log);
  var raw = log.map(function(item) {
    return '<div class="chat-text msg-send">'+$('#dummy').text(item).html()+'</div>';
  });

  $('.msg-insert').html(raw.join(''));
}

function chatHandleEnter(evt) {
  if (evt.which == 13) {
    evt.preventDefault();
    appendChatLog($('#msgarea').val());
    showChatLog();
    $('#msgarea').val("");
  }
}

function populateThemeSelection() {
  var initial = '['+
    '{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},'+
    '{"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},'+
    '{"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},'+
    '{"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},'+
    '{"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},'+
    '{"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},'+
    '{"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},'+
    '{"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},'+
    '{"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},'+
    '{"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},'+
    '{"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}'+
  ']';

  window.localStorage.setItem('themeList', initial);
}

function applyTheme(id) {
  var themes = JSON.parse(window.localStorage.getItem('themeList'));
  var theme = themes.filter(function(item) { return item.id == id; })[0];
  window.localStorage.setItem('selectedTheme', JSON.stringify(theme));
  realApplyTheme();
}

function realApplyTheme() {
  var theme = JSON.parse(window.localStorage.getItem('selectedTheme'));
  $('body').css('background-color', theme.bcgColor);
  $('#calc-buttons').css('color', theme.fontColor);
}

$( document ).ready(function() {
  print = document.getElementById('print');
  showChatLog();
  populateThemeSelection();

  $('#msgarea').keydown(chatHandleEnter);
  $('.my-select').select2({
    'data': JSON.parse(window.localStorage.getItem('themeList'))
  });
  $('.apply-button').click(function(){
    applyTheme($('.my-select').val());
  });
  realApplyTheme();
});