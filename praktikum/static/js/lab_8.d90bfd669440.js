window.fbAsyncInit = function() {
  FB.init({
    appId      : '762185610648842',
    cookie     : true,
    xfbml      : true,
    version    : 'v2.11'
  });

  FB.getLoginStatus(function(response) {
    render(response.status === 'connected');

  });
};

// Call init facebook. default dari facebook
(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

const render = loginFlag => {
  if (loginFlag) {
    getUserData(user => {
      $('#loginButton').attr('hidden', true);
      $('#lab8').html(
        '<div class="profile card">' +
          '<div class="header">' +
            '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
            '<div class="row">' +
              '<div>' +
                '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
                '<div class="data">' +
                  '<h1>' + user.name + '</h1>' +
                  '<h2>' + user.about + '</h2>' +
                '</div>' +
              '</div>' +
              '<button class="logout" onclick="facebookLogout()">Logout</button>' +
            '</div>' +
          '</div>' +
        '</div>' +

        '<div class="status card">' +
        '<textarea id="postInput" placeholder="Ketik Status Anda" maxrow="3"></textarea>' +
        '<button class="postStatus" onclick="postStatus()">Post ke Facebook</button>' +
        '</div>'
      );

      // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
      // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
      // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
      // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
      getUserFeed(feed => {
        feed.data.map(value => {
          // Render feed, kustomisasi sesuai kebutuhan.
          if (value.message && value.story) {
            $('#lab8').append(
              '<div class="feed card" id="'+value.id+'">' +
                '<h1>' + value.message + '</h1>' +
                '<h2>' + value.story + '</h2>' +
                '<button onclick="deleteStatus(\''+value.id+'\')">delete</button>'+
              '</div>'
            );
          } else if (value.message) {
            $('#lab8').append(
              '<div class="feed card" id="'+value.id+'">' +
                '<h1>' + value.message + '</h1>' +
                '<button onclick="deleteStatus(\''+value.id+'\')">delete</button>'+
              '</div>'
            );
          } else if (value.story) {
            $('#lab8').append(
              '<div class="feed card" id="'+value.id+'">' +
                '<h2>' + value.story + '</h2>' +
                '<button onclick="deleteStatus(\''+value.id+'\')">delete</button>'+
              '</div>'
            );
          }
        });
      });
    });
  } else {
    // Tampilan ketika belum login
    $('#lab8').html('<button class="login" onclick="facebookLogin()">Login</button>');
  }
};

const facebookLogin = () => {
  FB.login(function(response){
    console.log(response);
    render(true);
  }, {scope:'public_profile,user_posts,publish_actions,email,user_about_me'})
};

const facebookLogout = () => {
  FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
      FB.logout();
    }
  });
};

const getUserData = (fun) => {
  FB.api('/me?fields=name,cover,picture,gender,email,about', 'GET', function (response){
    fun(response);
  });
};

const getUserFeed = (fun) => {
  FB.api('/me/feed', 'GET', function (response){
    fun(response);
  });
};

const postFeed = (message) => {
  FB.api('/me/feed', 'POST', { message: message }, () => {
    render(true);
  });
};

const postStatus = () => {
  const message = $('#postInput').val();
  postFeed(message);
};

const deleteStatus = (id) => {
  FB.api(id, 'DELETE', () => {
    $('#'+id).remove();
  });
};
