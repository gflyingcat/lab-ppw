from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper

# Create your tests here.
class Lab7UnitTest(TestCase):

  def test_csuiapi_using_wrong_password(self):
    tmp = CSUIhelper.instance
    CSUIhelper.instance = None
    with self.assertRaises(Exception):
      helper = CSUIhelper('lol', 'lol')
    CSUIhelper.instance = tmp

  def test_auth_param_returning_things(self):
    CSUIhelper().instance.get_auth_param_dict()
    self.assertEqual(
      CSUIhelper().instance.get_client_id(),
      'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG')
    x, y = CSUIhelper().instance.get_mahasiswa_list()

  def test_using_index(self):
    found = resolve('/lab-7/')
    self.assertEqual(found.func, index)

  def test_friend_feature_is_working(self):
    resp1 = Client().post('/lab-7/add-friend/', {'name':'lol', 'npm':'123'}).content.decode('utf8')
    resp6 = Client().post('/lab-7/add-friend/', {'name':'lol', 'npm':'123'}).content.decode('utf8')
    self.assertIn('already friend', resp6)
    resp2 = Client().get('/lab-7/').content.decode('utf8')
    self.assertIn('lol', resp2)
    resp3 = Client().get('/lab-7/get-friend-list/').content.decode('utf8')
    self.assertIn('lol', resp3)
    resp3 = Client().get('/lab-7/friend-list/')
    self.assertEqual(resp3.status_code, 200)
    resp4 = Client().post('/lab-7/validate-npm/', {'npm':'123'}).content.decode('utf8')
    self.assertIn('true', resp4)
    resp5 = Client().get('/lab-7/delete-friend/1/')
    self.assertEqual(resp5.status_code, 200)




