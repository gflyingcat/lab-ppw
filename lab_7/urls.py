from django.conf.urls import url
from lab_7.views import (
    index, add_friend, validate_npm, friend_list_json, delete_friend, friend_list
)

urlpatterns = [
    url(r'^$',
        index,
        name='index'),
    url(r'^friend-list/$',
        friend_list,
        name='friend-list'),
    url(r'^add-friend/$',
        add_friend,
        name='add-friend'),
    url(r'^validate-npm/$',
        validate_npm,
        name='validate-npm'),
    url(r'^delete-friend/(?P<friend_id>[0-9]+)/$',
        delete_friend,
        name='delete-friend'),
    url(r'^get-friend-list/$',
        friend_list_json,
        name='get-friend-list')
]