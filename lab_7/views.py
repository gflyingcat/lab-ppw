from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Friend
from django.forms import model_to_dict
from .api_csui_helper.csui_helper import CSUIhelper
import os

response = {}
csui_helper = CSUIhelper()


def index(request):
    current_page = request.GET.get('page', 1)
    mahasiswa_list, total_page = csui_helper.instance.get_mahasiswa_list(current_page)
    friend_list = Friend.objects.all()
    html = 'lab_7/lab_7.html'
    response['mahasiswa_list'] = mahasiswa_list
    response['friend_list'] = friend_list
    response['pages'] = range(1, total_page+1)
    response['current_page'] = current_page
    return render(request, html, response)


def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

def friend_list_json(request): # update
    friends = [model_to_dict(obj) for obj in Friend.objects.all()]
    return JsonResponse({"results": friends}, content_type='application/json')

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        npm = request.POST['npm']
        if (not Friend.objects.filter(npm=npm).exists()):
            name = request.POST['name']
            friend = Friend(friend_name=name, npm=npm)
            friend.save()
            return JsonResponse(model_to_dict(friend))
        else:
            return JsonResponse({'error': 'already friend'})


@csrf_exempt
def delete_friend(request, friend_id):
    Friend.objects.filter(id=friend_id).delete()
    return JsonResponse({'error': 'success'})


@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': Friend.objects.filter(npm=npm).exists()
    }
    return JsonResponse(data)
