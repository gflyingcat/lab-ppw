from .models import Pengguna, MovieKu
from .omdb_api import get_detail_movie


def check_movie_in_database(request, model, kode_movie):
    kode_identitas = get_data_user(request, 'kode_identitas')
    pengguna = Pengguna.objects.get(kode_identitas=kode_identitas)
    return model.objects.filter(pengguna=pengguna, kode_movie=kode_movie).exists()


def check_movie_in_session(request, key, kode_movie):
    movies = request.session.get(key, [])
    if kode_movie in movies:
        return True
    return False


# def add_item_to_database(request, model, id):
#     kode_identitas = get_data_user(request, 'kode_identitas')
#     pengguna = Pengguna.objects.get(kode_identitas=kode_identitas)
#     movieku = model()
#     movieku.kode_movie = id
#     movieku.pengguna = pengguna
#     movieku.save()


# def add_item_to_session(request, key, id):
#     movies = request.session.get(key, [])
#     if id not in movies:
#         movies.append(id)
#         request.session[key] = movies


def get_data_user(request, tipe):
    return request.session.get(tipe, None)


def create_new_user(request):
    nama = get_data_user(request, 'user_login')
    kode_identitas = get_data_user(request, 'kode_identitas')

    pengguna = Pengguna()
    pengguna.kode_identitas = kode_identitas
    pengguna.nama = nama
    pengguna.save()

    return pengguna


def get_parameter_request(request):
    judul = request.GET.get('judul', '-')
    tahun = request.GET.get('tahun', '-')
    return judul, tahun


def save_movies_to_database(pengguna, model, list_movie_id):
    #looping get id, cek apakah exist berdasarkan user, jika tidak ada, maka tambah
    for movie_id in list_movie_id:
        if not model.objects.filter(pengguna = pengguna, kode_movie = movie_id).exists():
            new_movie = MovieKu()
            new_movie.pengguna = pengguna
            new_movie.kode_movie = movie_id
            new_movie.save()


def get_my_movies_from_database(request, model):
    kode_identitas = get_data_user(request, 'kode_identitas')
    pengguna = Pengguna.objects.get(kode_identitas=kode_identitas)
    items = model.objects.filter(pengguna=pengguna)
    return [ item.kode_movie for item in items ]


def get_my_movies_from_session(request):
    return request.session.get('movies', []), request.session.get('watched', [])


def get_list_movie_from_api(my_list):
    return [ get_detail_movie(movie) for movie in my_list]


def is_logged_in(request):
    return 'user_login' in request.session.keys()

