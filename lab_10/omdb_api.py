import requests

URL = 'http://www.omdbapi.com/'
API_KEY = "8654dfa1"

def search_movie(judul, tahun):
    params={
        's': judul,
        'apikey': API_KEY,
        'page': 1,
    }

    if tahun != '-':
        params['y'] = tahun

    req = requests.get(URL, params=params).json()
    movies = []

    req_status = req['Response'] == 'True'
    if not req_status:
        return movies

    count_results = int(req['totalResults'])
    # ambil 3 page ( ceil(count / 10) )
    pages = min(int(count_results + 9) // 10, 3)
    data_exist = True

    for page in range(pages):
        params['page'] = page + 1
        req = requests.get(URL, params=params).json()
        get_datas = req['Search']
        for data in get_datas:
            movies.append(data)

    return movies


def get_detail_movie(movie_id):
    req = requests.get(URL, params={'i': movie_id, 'apikey': API_KEY}).json()
    return req
