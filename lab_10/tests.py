import os
import environ
import requests

from django.urls import reverse
from django.test import TestCase
from django.test import Client
from django.urls import resolve

from lab_10.csui_helper import get_access_token, get_client_id, verify_user, get_data_user
from praktikum.settings import PROJECT_ROOT

env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

class Lab10UnitTest(TestCase):
    def test_api_csui(self):
        username = env('SSO_USERNAME')
        password = env('SSO_PASSWORD')

        access_token = get_access_token(username, password)
        self.assertNotEqual(access_token, None)
        self.assertEqual(get_access_token('lol', 'lol'), None)

        self.assertEqual(get_client_id(), 'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG')

        self.assertNotEqual(verify_user(access_token), None)
        self.assertNotEqual(get_data_user(access_token, '1606862753'), None)

    def init_login(self):
        username = env('SSO_USERNAME')
        password = env('SSO_PASSWORD')
        self.client.post(reverse('lab-10:auth_login'), data={
            'username': username,
            'password': password,
        }, follow=True).content.decode('utf-8')
        self.assertEqual(self.client.get(reverse('lab-10:dashboard')).status_code, 200)

    def test_custom_auth(self):
        response = self.client.post(reverse('lab-10:auth_login'), data={
            'username': 'lol',
            'password': 'lol',
        }, follow=True).content.decode('utf-8')
        self.assertIn('Username atau password salah', response)

        username = env('SSO_USERNAME')
        password = env('SSO_PASSWORD')
        response = self.client.post(reverse('lab-10:auth_login'), data={
            'username': username,
            'password': password,
        }, follow=True).content.decode('utf-8')
        self.assertIn('Anda berhasil login', response)

        response = self.client.get(reverse('lab-10:auth_logout'), follow=True).content.decode('utf-8')
        self.assertIn('Anda berhasil logout. Semua session Anda sudah dihapus', response)

    def test_dashboard_is_available(self):
        self.init_login()

        response = self.client.get(reverse('lab-10:movie_list'))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('lab-10:api_search_movie', kwargs={'judul':'-', 'tahun':'-'})).content.decode('utf-8')
        self.assertNotIn('LSKFALJSFJSA', response)

        response = self.client.get(reverse('lab-10:api_search_movie', kwargs={'judul':'chef', 'tahun':'-'})).content.decode('utf-8')
        self.assertIn('A Chef in Love', response)

        response = self.client.get(reverse('lab-10:api_search_movie', kwargs={'judul':'LSKFALJSFJSA', 'tahun':'-'})).content.decode('utf-8')
        self.assertNotIn('LSKFALJSFJSA', response)

        response = self.client.get(reverse('lab-10:api_search_movie', kwargs={'judul':'chef', 'tahun': '1996'})).content.decode('utf-8')
        self.assertIn('A Chef in Love', response)

    def test_dashboard_redirects_if_not_login(self):
        self.assertEqual(self.client.get(reverse('lab-10:dashboard')).status_code, 302)

    def test_movie_detail(self):
        response = self.client.get(reverse('lab-10:movie_detail', kwargs={'id': 'tt0117050'})).content.decode('utf-8')
        self.assertIn('Add to Watch Later', response)

        self.init_login()
        response = self.client.get(reverse('lab-10:movie_detail', kwargs={'id': 'tt0117050'})).content.decode('utf-8')
        self.assertIn('Add to Watch Later', response)

    def test_add_watch_later(self):
        self.client.get(reverse('lab-10:add_watch_later', kwargs={'id': 'tt0117050'}))
        response = self.client.get(reverse('lab-10:list_watch_later')).content.decode('utf-8')
        self.assertIn('A Chef in Love', response)

        self.init_login()

        response = self.client.get(reverse('lab-10:add_watch_later', kwargs={'id': 'tt0356634'}))
        print(response.content.decode('utf-8'))

        response = self.client.get(reverse('lab-10:list_watch_later')).content.decode('utf-8')
        self.assertIn('A Chef in Love', response)
        self.assertIn('Garfield', response)

    def test_add_watched(self):
        self.client.get(reverse('lab-10:add_watch_later', kwargs={'id': 'tt0117050'}))
        response = self.client.get(reverse('lab-10:list_watch_later')).content.decode('utf-8')
        self.assertIn('A Chef in Love', response)
        response = self.client.get(reverse('lab-10:movie_detail', kwargs={'id': 'tt0117050'})).content.decode('utf-8')
        self.assertNotIn('Add to Watch Later', response)
        self.client.get(reverse('lab-10:add_watched', kwargs={'id': 'tt0117050'})).content.decode('utf-8')

        response = self.client.get(reverse('lab-10:list_watch_later')).content.decode('utf-8')
        self.assertNotIn('A Chef in Love', response)
        response = self.client.get(reverse('lab-10:watched')).content.decode('utf-8')
        self.assertIn('A Chef in Love', response)

        self.init_login()
        self.client.get(reverse('lab-10:add_watch_later', kwargs={'id': 'tt0356634'}))
        response = self.client.get('/lab-10/movie/watched/add/tt0356634/')
        response = self.client.get(reverse('lab-10:list_watch_later')).content.decode('utf-8')
        # self.assertIn('A Chef in Love', response)
        # self.assertIn('Garfield', response)
        response = self.client.get(reverse('lab-10:watched')).content.decode('utf-8')
        # self.assertIn('Garfield', response)
        # self.assertIn('A Chef in Love', response)
