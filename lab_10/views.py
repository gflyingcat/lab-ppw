import json

from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from .models import WatchedList, MovieKu

from .omdb_api import get_detail_movie, search_movie
from .utils import *

response = {}

def get_data_session(request):
    if is_logged_in(request):
        response['author'] = get_data_user(request, 'user_login')


def set_active(section):
    response['section'] = section


def index(request):
    if not is_logged_in(request):
        response = {'author': get_data_user(request, 'user_login')}
        html = 'lab_10/login.html'
        return render(request, html, response)

    return HttpResponseRedirect(reverse('lab-10:dashboard'))


def dashboard(request):
    if not is_logged_in(request):
        return HttpResponseRedirect(reverse('lab-10:index'))

    kode_identitas = get_data_user(request, 'kode_identitas')
    response['author'] = get_data_user(request, 'user_login')
    response['kode_identitas'] = kode_identitas
    response['role'] = get_data_user(request, 'role')

    try:
        pengguna = Pengguna.objects.get(kode_identitas=kode_identitas)
    except Exception as e:
        pengguna = create_new_user(request)

    watch_later, watched = get_my_movies_from_session(request)
    save_movies_to_database(pengguna, MovieKu, watch_later)
    save_movies_to_database(pengguna, WatchedList, watched)

    set_active('dashboard')
    html = 'lab_10/dashboard.html'
    return render(request, html, response)


def movie_list(request):
    judul, tahun = get_parameter_request(request)
    urlDataTables = "/lab-10/api/movie/" + judul + "/" + tahun
    jsonUrlDT = json.dumps(urlDataTables)

    response['jsonUrlDT'] = jsonUrlDT
    response['judul'] = judul
    response['tahun'] = tahun
    response['movies'] = True
    get_data_session(request)

    set_active('movies')
    html = 'lab_10/movie/list.html'
    return render(request, html, response)


def movie_detail(request, id):
    response['id'] = id
    if is_logged_in(request):
        is_added = check_movie_in_database(request, MovieKu, id)
        is_watched = check_movie_in_database(request, WatchedList, id)
    else:
        is_added = check_movie_in_session(request, 'movies', id)
        is_watched = check_movie_in_session(request, 'watched', id)

    response['added'] = is_added
    response['watched'] = is_watched
    response['movie'] = get_detail_movie(id)
    set_active('movies')
    html = 'lab_10/movie/detail.html'
    return render(request, html, response)


def add_watch_later(request, id):
    if is_logged_in(request):
        kode_identitas = get_data_user(request, 'kode_identitas')
        pengguna = Pengguna.objects.get(kode_identitas=kode_identitas)

        movie = MovieKu.objects.filter(kode_movie=id).first()
        if movie is None:
            MovieKu.objects.create(pengguna=pengguna, kode_movie=id)
    else:
        watch_later, watched = get_my_movies_from_session(request)
        if id not in watch_later:
            watch_later.append(id)
        request.session['movies'] = watch_later

    return HttpResponseRedirect(reverse('lab-10:movie_detail', args=(id,)))


def add_to_watched(request, id):
    if is_logged_in(request):
        kode_identitas = get_data_user(request, 'kode_identitas')
        pengguna = Pengguna.objects.get(kode_identitas=kode_identitas)

        movie = MovieKu.objects.filter(kode_movie=id).first()
        if movie is not None:
            movie.delete()

        if not WatchedList.objects.filter(kode_movie=id).exists():
            WatchedList.objects.create(pengguna=pengguna, kode_movie=id)
    else:
        watch_later, watched = get_my_movies_from_session(request)
        if id in watch_later:
            watch_later.remove(id)

        if id not in watched:
            watched.append(id)

        request.session['movies'] = watch_later
        request.session['watched'] = watched

    return HttpResponseRedirect(reverse('lab-10:watched'))


def list_watch_later(request):
    get_data_session(request)
    moviesku = []
    if get_data_user(request, 'user_login'):
        moviesku = get_my_movies_from_database(request, MovieKu)
    else:
        moviesku, _ = get_my_movies_from_session(request)

    watch_later_movies = get_list_movie_from_api(moviesku)
    response['watch_later_movies'] = watch_later_movies

    set_active('watch_later')
    html = 'lab_10/movie/watch_later.html'
    return render(request, html, response)


def list_watched(request):
    get_data_session(request)
    watched = []
    if get_data_user(request, 'user_login'):
        watched = get_my_movies_from_database(request, WatchedList)
    else:
        _, watched = get_my_movies_from_session(request)

    watch_later_movies = get_list_movie_from_api(watched)
    response['watch_later_movies'] = watch_later_movies

    set_active('watched')
    html = 'lab_10/movie/watch_later.html'
    return render(request, html, response)


def api_search_movie(request, judul, tahun):
    if judul == "-" and tahun == "-":
        items = []
    else:
        search_results = search_movie(judul, tahun)
        items = search_results

    dataJson = json.dumps({"dataku": items})
    mimetype = 'application/json'
    return HttpResponse(dataJson, mimetype)
