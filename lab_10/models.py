from django.db import models


class Pengguna(models.Model):
    kode_identitas = models.CharField('Kode Identitas', max_length=20, primary_key=True, )
    nama = models.CharField('Nama', max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class MovieKu(models.Model):
    pengguna = models.ForeignKey(Pengguna)
    kode_movie = models.CharField("Kode Movie", max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class WatchedList(models.Model):
    pengguna = models.ForeignKey(Pengguna, on_delete=models.CASCADE)
    kode_movie = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
