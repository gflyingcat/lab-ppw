from django.conf.urls import url
from .views import index, add_todo, remove_todo

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_todo', add_todo, name='add_todo'),
    url(r'^remove_todo/(?P<id>[0-9]+)/', remove_todo, name='remove_todo'),
]
