from django.shortcuts import render
from lab_2.views import landing_page_content
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message

# Create your views here.
response = {'author': "Fata Nugraha"}
about_me = ['foo', 'bar', 'baz', '2000', '322', '666']


def index(request, form=None):
    response['content'] = landing_page_content
    html = 'lab_4.html'
    if form is None:
        response['message_form'] = Message_Form
    else:
        response['message_form'] = form
    response['about_me'] = about_me
    return render(request, html, response)


def message_post(request):
    form = Message_Form(request.POST or None)

    if request.method == 'POST':
        if form.is_valid():
            response['name'] = "Anonymous"
            if request.POST['name'] != '':
                response['name'] = request.POST['name']

            response['email'] = "Anonymous"
            if request.POST['email'] != '':
                response['email'] = request.POST['email']

            response['message'] = request.POST['message']

            message = Message(
                name=response['name'],
                email=response['email'],
                message=response['message']
            )
            message.save()
            html = 'form_result.html'
            return render(request, html, response)
        else:
            return index(request, form)
    else:
        return HttpResponseRedirect('/lab-4/')


def message_table(request):
    message = Message.objects.all()
    response['message'] = message
    html = 'table.html'
    return render(request, html, response)
