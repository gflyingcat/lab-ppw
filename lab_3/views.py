from django.shortcuts import render, redirect
from .models import Diary
from datetime import datetime
from django import forms
import pytz
# Create your views here.


def index(request, old_input={}, errors={}):
    diary_dict = Diary.objects.all().values()
    return render(request, 'to_do_list.html', {
        'diary_dict': convert_queryset_into_json(diary_dict),
        'old': old_input,
        'errors': errors,
    })


def add_activity(request):
    if request.method == 'POST':
        form = DiaryForm(request.POST)
        if form.is_valid():
            date = form.cleaned_data['date']
            Diary.objects.create(
                date=date.replace(tzinfo=pytz.UTC),
                activity=form.cleaned_data['activity'])
            return redirect('/lab-3/')
        else:
            return index(request, request.POST, form.errors)


def convert_queryset_into_json(queryset):
    ret_val = []
    for data in queryset:
        ret_val.append(data)
    return ret_val


class DiaryForm(forms.Form):
    activity = forms.CharField()
    date = forms.DateTimeField(input_formats=['%Y-%m-%dT%H:%M'])
