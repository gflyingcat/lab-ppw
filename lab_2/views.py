from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'Somebody once told me the world is gonna roll me.'+\
    'I aint the sharpest tool in the shed.'+\
    'She was looking kind of dumb with her finger and her thumb.'+\
    'In the shape of an L on her forehead.'

def index(request):     
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)
