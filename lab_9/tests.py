import os
import environ
import requests

from django.urls import reverse
from django.test import TestCase
from django.test import Client
from django.urls import resolve

from lab_9.api_enterkomputer import get_drones, get_soundcards, get_opticals
from lab_9.csui_helper import get_access_token, get_client_id, verify_user, get_data_user
from praktikum.settings import PROJECT_ROOT

env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

class Lab9UnitTest(TestCase):
  def test_api_enterkomputer(self):
    self.assertNotEqual(get_drones(), 0)
    self.assertNotEqual(get_soundcards(), 0)
    self.assertNotEqual(get_opticals(), 0)

  def test_api_csui(self):
    username = env('SSO_USERNAME')
    password = env('SSO_PASSWORD')

    access_token = get_access_token(username, password)
    self.assertNotEqual(access_token, None)
    self.assertEqual(get_access_token('lol', 'lol'), None)

    self.assertEqual(get_client_id(), 'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG')

    self.assertNotEqual(verify_user(access_token), None)
    self.assertNotEqual(get_data_user(access_token, '1606862753'), None)

  def test_custom_auth(self):
    username = env('SSO_USERNAME')
    password = env('SSO_PASSWORD')

    response = self.client.post(reverse('lab-9:auth_login'), data={
      'username': 'lol',
      'password': 'lol',
    }, follow=True)
    self.assertIn('salah', response.content.decode('utf8'))

    # print(dir(self.client))
    response = self.client.get(reverse('lab-9:index'))
    self.assertIn('Login', response.content.decode('utf8'))
    response = self.client.get(reverse('lab-9:profile'), follow=True)
    self.assertIn('Login', response.content.decode('utf8'))

    self.client.post(reverse('lab-9:auth_login'), data={
      'username': username,
      'password': password,
    })

    response = self.client.get(reverse('lab-9:index'), follow=True)

    self.assertEqual('Login' not in response.content.decode('utf8'), True)

    self.assertEqual(self.client.session.get('drones', None), None)

    self.client.get(reverse('lab-9:add_session_item', kwargs={'key': 'drones', 'id': 1}))
    self.client.get(reverse('lab-9:add_session_item', kwargs={'key': 'drones', 'id': 2}))
    self.assertEqual(len(self.client.session['drones']), 2)

    self.client.get(reverse('lab-9:del_session_item', kwargs={'key': 'drones', 'id': 1}))
    self.assertEqual(len(self.client.session['drones']), 1)

    self.client.get(reverse('lab-9:clear_session_item', kwargs={'key': 'drones'}))
    self.assertEqual(self.client.session.get('drones', None), None)
    self.client.get(reverse('lab-9:auth_logout'), follow=True)

  def test_cookie(self):
    username = env('SSO_USERNAME')
    password = env('SSO_PASSWORD')

    response = self.client.get('/lab-9/cookie/login/')
    self.assertIn('Login', response.content.decode('utf8'))

    response = self.client.get('/lab-9/cookie/auth_login/')
    self.assertEqual(response.status_code, 302)

    response = self.client.get('/lab-9/cookie/profile', follow=True)
    self.assertIn('Login', response.content.decode('utf8'))

    response = self.client.post('/lab-9/cookie/auth_login/', data={
      'password': 'lol',
      'username': 'lol',
    }, follow=True)
    self.assertIn('Salah', response.content.decode('utf8'))

    response = self.client.post('/lab-9/cookie/auth_login/', data={
      'password': 'ptest',
      'username': 'utest',
    }, follow=True)
    self.assertNotIn('Salah', response.content.decode('utf8'))

    response = self.client.get('/lab-9/cookie/login/', follow=True)
    self.assertNotIn('Login', response.content.decode('utf8'))

    self.client.cookies['user_password'] = 'lol'

    response = self.client.get('/lab-9/cookie/profile', follow=True)
    self.assertIn('Kamu tidak punya akses', response.content.decode('utf8'))

    self.client.get('/lab-9/cookie/clear', follow=True)