from django.test import TestCase
from django.urls import resolve
from django.test import Client

from lab_8.views import index

class Lab8UnitTest(TestCase):
    def test_using_index(self):
        found = resolve('/lab-8/')
        self.assertEqual(found.func, index)
        response = Client().get('/lab-8/')
        self.assertEqual(response.status_code, 200)