from django.shortcuts import render

def index(request):
  template = 'lab_8/base.html'
  return render(request, template, {})
