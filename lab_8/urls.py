from django.conf.urls import url
from lab_8.views import index

urlpatterns = [
  url(r'^$', index, name='index'),
]